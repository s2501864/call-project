FROM debian:stable-slim

ADD models/ models/
ADD backend/ backend/

RUN apt update && apt install -y python pip
RUN pip install -r backend/requirements.txt
RUN python3 -m spacy download en
WORKDIR "backend/"
