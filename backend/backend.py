from happytransformer import HappyTextToText
from happytransformer import TTTrainArgs
from happytransformer import TTSettings
from happytransformer import HappyGeneration
import errant
from flask import Flask
from flask import render_template
from flask import request
from flask_cors import CORS, cross_origin
import json
import os

def initialize():
    happy_tt_german = HappyTextToText("T5", "t5-base", load_path="../models/t5-german/")
    happy_tt_spanish = HappyTextToText("T5", "t5-base", load_path="../models/t5-spanish/")
    beam_settings =  TTSettings(num_beams=5, min_length=1, max_length=20)
    annotator = errant.load('en')
    return happy_tt_german, happy_tt_spanish, beam_settings, annotator

def correct_sentence(happy_tt, beam_settings, annotator, text):
    output = happy_tt.generate_text(text, args=beam_settings)
    orig = annotator.parse(text)
    corr = annotator.parse(output.text)
    edits = annotator.annotate(orig, corr)
    return output.text, edits

app = Flask(__name__)
CORS(app, resources=r'/*')
happy_tt_german, happy_tt_spanish, beam_settings, annotator = initialize()

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
    app.run(ssl_context='adhoc')

'''
Will render HTML page inside the template folder
'''
@app.get("/")
def render_page():
    return render_template('index.html')

@app.post("/german")
@cross_origin()
def correct_german():
    text = request.form['input']
    print("We got the input: {}".format(text))
    output, edits = correct_sentence(happy_tt_german, beam_settings, annotator, text)
    print("The corrected output is: {}".format(output))
    errlist = []
    for e in edits:
        error = "{} {} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str, e.type)
        errlist.append(error)

    data = {
        "model": "German",
        "input": text,
        "output": output,
        "error": errlist
    }

    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response 

@app.post("/spanish")
def correct_spanish():
    text = request.form['input']
    print("We got the input: {}".format(text))
    output, edits = correct_sentence(happy_tt_spanish, beam_settings, annotator, text)
    print("The corrected output is: {}".format(output))
    errlist = []
    for e in edits:
        error = "{} {} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str, e.type)
        errlist.append(error)
    
    data = {
        "model": "Spanish",
        "input": text,
        "output": output,
        "error": errlist
    }
    
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response
