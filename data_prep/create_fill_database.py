#!/usr/bin/env python3

import re
import sqlite3
import pandas as pd
import random

'''
Creates database and tables if they do not exist
Returns connection to database
'''
def database_setup(database):
    conn = sqlite3.connect(database)
    conn.execute("CREATE TABLE IF NOT EXISTS dataset(lineNr INTEGER PRIMARY KEY, fullLine VARCHAR, nativeLang VARCHAR, input VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS errors(errorID INTEGER, error VARCHAR, type VARCHAR)")
    conn.commit()
    return conn

'''
Reads the original dataset and inserts the values into the database
'''
def read_dataset(dataset):
    conn = database_setup("dataset_errors.db")

    dataset = pd.read_csv(dataset)

    for index, row in dataset.iterrows():
        conn.execute("INSERT INTO dataset VALUES(?,?,?,?,?)", (int(index), str(row), str(row['native']), "NULL", random.random(),))

    conn.commit()
    conn.close()

'''
Reads the error classifications and inserts them into the database, associating with the original data
'''
def read_errors(file):
    conn = database_setup("dataset_errors.db")
    f=open(file)
    lines = f.readlines()

    lineIndex = 0
    errorIDs = []
    for line in lines:
        input = re.search("\[(Input)\] .+?\[", line)
        if (input == None):
            continue

        input = input.group()
        input = input.replace("[Input]", "")
        input = input.replace("[", "")

        input = input.replace("\n", "")
        input = input.replace("\r", "")
        input = input.replace("\^M", "")

        edits = re.search("\[(Edits)\] .+?\]", line)
        if (edits == None):
            continue

        edits = edits.group()
        #print("\nEdits: \n" + edits)

        errors = re.findall("\(\'.+?\)", edits)

        errorId = int(random.random()*1000000)
        while errorId in errorIDs:
            errorId = int(random.random()*1000000)
        errorIDs.append(errorId)

        conn.execute("UPDATE dataset set input=?,errorID=? where lineNr=?", (str(input), errorId, lineIndex))
        lineIndex += 1
        conn.commit()

        counter = 0
        for error in errors:
            counter += 1
            error_type = re.search("\(\'.+?\'", error)
            if (error_type != None):
                error_type = error_type.group()
                error_type = error_type.replace("(", "")
                error_type = error_type.replace("'", "")
                conn.execute("INSERT INTO errors VALUES(?,?,?)", (int(errorId), str(error), str(error_type)))

    conn.commit()
    conn.close()

'''
Creates the table error_stats for the statistics of language and error type frequency
'''
def fill_error_stats():
    conn = sqlite3.connect('dataset_errors.db')

    conn.execute("CREATE TABLE IF NOT EXISTS error_stats(nativeLang VARCHAR, type VARCHAR, count INTEGER)")
    cur = conn.cursor()

    cur.execute("SELECT DISTINCT type from errors")
    errors = cur.fetchall()

    cur.execute("SELECT DISTINCT nativeLang from dataset")
    languages = cur.fetchall()

    for l in languages:
        for e in errors:
            cur.execute("SELECT count(type) from dataset, errors WHERE nativeLang=? AND type=? AND dataset.errorID=errors.errorID", (l[0], e[0]))
            res = cur.fetchall()
            cur.execute("INSERT INTO error_stats VALUES(?,?,?)", (l[0], e[0], res[0][0]))
            conn.commit()
            print("\n")
            cur.execute("SELECT * from error_stats")
            errors = cur.fetchall()
            print(errors)

    conn.close()

'''
1. Read in the original dataset
2. Fill in the errors into the table
'''
read_dataset("processed_dataset.csv")
read_errors('output2.txt')
