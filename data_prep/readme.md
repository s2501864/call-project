# Introduction
General workflow:  
- clean up provided dataset, drop the non-english submissions
- use Gramformer on the Uni server to get error classifications
- clean up the result-set and insert it into sqlite database (seems a bit nicer than working with csv files)
- perform some cleanup on the database
- export error frequencies as csv from the database and visualize them in jupyter

A lot of the code is quite messy, sorry. 

# File overview 
**Python files:**
- Gramformer_local.py  
Python file to use gramformer locally, not recommended except if you have proper hardware

- create_fill_database.py  
Creates an sqlite database and fills in user information and the error type

- clean_database.py  
Cleans up various columns in the database

**Jupyter Lab files:**
- Data_prep.ipynb  
Performs the Gramformer analysis on the provided dataset. A bit tricky to set up.

- Error_frequency.ipynb  
Makes some nice graphs for the error frequencies

**CSV files:**
- processed_dataset.csv  
Original dataset without the non-english targets

- query_results.csv  
Query result of:  
```SQL
SELECT * FROM error_stats WHERE count > 0;
```

**txt files**
- output.txt  
Output from the Gramformer model. So includes the input sentence, as well as error correction and classification.

- output2.txt  
Cleaned up version of output.txt. Converted with dos2unix to remove newlines.

**db files:**
- dataset_errors.db  
Sqlite database with original submission, native language, errors, ...

# Gramformer setup
## Installation 
Follow the installation steps here: https://github.com/PrithivirajDamodaran/Gramformer/#quick-start and install missing dependencies.
You need a happyface api key and set it in the environment. Look in the guide on how to do it.

## Fixing 'Can't find model 'en' error'
This shouldn't happen with the huggingface-cli step done before
```shell
python -m spacy download en
python -m spacy download en_core_web_sm
```

```python
import spacy
spacy.load("en")
```
**Source**: https://github.com/PrithivirajDamodaran/Gramformer/issues/25

# Error frequency analysis
## Export error frequencies from sqlite 
I saved all errors in the sqlite database in the table error_stats. So exporting the data with:
``` sql
.headers on
.mode csv
.once query_results.csv
SELECT * FROM error_stats WHERE count>0;
```

The resulting csv file is then used by _Error_frequency.ipynb_ to make some nice graphs.

**Source**: https://database.guide/export-sqlite-query-results-to-csv-file/
