{ pkgs ? import <nixpkgs> {} }:

with pkgs;
let
  python-with-my-packages = pkgs.python3.withPackages (p: with p; [
    # other python packages you want
    numpy
    pandas
    scipy
    jupyterlab
    matplotlib
  ]);
in
python-with-my-packages.env # replacement for pkgs.mkShell
