#!/usr/bin/env python3
import sqlite3

def database_setup(database):
    conn = sqlite3.connect(database)
    return conn
'''
    conn.execute("CREATE TABLE IF NOT EXISTS dataset(lineNr INTEGER PRIMARY KEY, fullLine VARCHAR, nativeLang VARCHAR, input VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS errors(errorID INTEGER, error VARCHAR, type VARCHAR)")
'''

def clean_lang():
    conn = sqlite3.connect('dataset_errors.db')
    cur = conn.cursor()

    #cur.execute("SELECT dataset.input, errors.type from dataset,errors where lineNr=? and dataset.errorID=errors.errorID", (str(i)))
    cur.execute("SELECT lineNr, nativeLang from dataset")
    res = cur.fetchall()
    for col in res:
        if col[1].find("br/") >= 0:
            substring = col[1].split("<br/")
            print(substring[0])
            conn.execute("UPDATE dataset SET nativelang=? WHERE lineNR=?", (substring[0], col[0]))

            conn.commit()
            #print("Input for line {}: {}".format(str(i), str(res)))
            print("\n")

            conn.close()

def fix_multiline_input():
    file = 'output.txt'
    output = 'output2.txt'

    f=open(file)
    outf=open(output, "w")

    lines = f.readlines()

    lineIndex = 0
    templine = ''
    multiline = False

    for line in lines:
        # we have both input and output in the same line
        print(line)
        if (line.find("[Input]") >= 0 and line.find("[Edits]") >= 0):
            print("FOUND START OF LINE WITH EDIT")
            if multiline:
                print("WRITING TO FILE")
                outf.write(templine.replace("\n", ""))
                templine = ''
                multiline = False

                outf.write(line)

                # we only have the first part in the line
            elif (line.find("[Input]") >= 0 and line.find("[Edits]") == -1):
                print("FOUND START OF LINE WITHOUT EDIT")
                if multiline:
                    print("WRITING TO FILE")
                    outf.write(templine.replace("\n", ""))
                    templine = ''
                    multiline = False

                    templine = templine + ' ' + line.replace("\n", "")
                    multiline = True

                    # we have found the end of the line
                elif (line.find("[Input]") == -1 and line.find("[Edits]") >= 0):
                    print("FOUND EDIT")
                    templine = templine + ' ' + line.replace("\n", "")

                    # any line without both
                else:
                    print("FOUND LINE WITHOUT BOTH")
                    templine = templine + ' ' + line.replace("\n", "")

    f.close()
    outf.close()
