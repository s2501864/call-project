# CALL project overview

## Repository content
**backend/**:  
Contains the code of the backend, including the HTML frontend.

**data_prep/**:  
Contains all code to perform in the initial error frequency analysis on the original given dataset.

**dataset_gen/**:  
Contains code to generate our own dataset based on the observed error frequencies.

**model-training/**:  
Contains code to train the model on the generated dataset.

**models/**:  
Contains the saved T5 models.

**webpage/**:  
Initial design of the website.

## A note on using docker
Packaging the project into a docker container works, but currently the frontend has issues due to self-signed certificates and calls to an unsecured API. We did not have enough time to properly fix this, but after the certificate problem is solved the project is fully runnable inside docker.  
Note: You need to modify the start parameters of the docker process to raise the size limit of the docker container, as it will be larger than 10GB. 
