# Dataset Creation

## Starting point
We have the error frequencies mapped to native languages. Now we want to create a model that takes the error frequencies into account. 

## Tagged error corruption
Some researchers at Google outlined a way to create an artificial dataset using a tagged grammatical error corruption model: https://arxiv.org/abs/2106.03830v2

I was not able to properly replicate that model, but instead used the dataset they have created as a base (C4_200M): https://ai.googleblog.com/2021/08/the-c4200m-synthetic-dataset-for.html

This dataset contains sentence pairs, one grammatically correct and one grammatically corrupt. In order to extract single sentence pairs I created a sqlite database and inserted the data there. 

## Augmenting this dataset according to our error frequencies
I used the ERRANT toolkit (https://github.com/chrisjbryant/errant) to extract the grammatical errors present in the C4_200M dataset. Because of time and computational constraints I was only able to analyze a subset of the data, but I was able to classify which sentences contained which kind of error. 

Using the error frequencies we found earlier I then extracted sentences from the original dataset and created my own dataset. I only used Spanish as a starting point, plan is to create a dataset for each native language. 
