#!/usr/bin/env python3

from datasets import load_dataset
import errant
import sqlite3
import random

def database_setup():
    conn = sqlite3.connect("c4_200m_errors.db")
    conn.execute("CREATE TABLE IF NOT EXISTS dataset(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS errors(errorID INTEGER, error VARCHAR, type VARCHAR)")
    conn.execute("CREATE TABLE IF NOT EXISTS spanish(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS german(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.commit()
    return conn

def fill_database():
    dataset = load_dataset("liweili/c4_200m")
    conn = database_setup()

    for i in range(len(dataset['train']['input'])):
        orig = dataset['train']['input'][i]
        corr = dataset['train']['output'][i]

        errorId = int(random.random()*1000000)
        conn.execute("INSERT INTO dataset VALUES(?,?,?)", (str(orig), str(corr), int(errorId)))

        if i%10 == 0:
            print("Committed database")
            conn.commit()

        if i>100000:
            break

        print("Read line {}".format(i))

    conn.commit()
    conn.close()
    print("Execution finished")

def classify_errors():
    conn = database_setup()
    cur = conn.cursor()
    annotator = errant.load('en')
    
    for i in range(183894319):
        cur.execute("SELECT * from dataset where errorID = {}".format(str(i)))
        res = cur.fetchall()
        input = res[0][0]
        output = res[0][1]
        errorID = res[0][2]
        
        orig = annotator.parse(input)
        corr = annotator.parse(output)
        edits = annotator.annotate(orig, corr)
        
        for e in edits:
            error = "{} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str)
            conn.execute("INSERT INTO errors VALUES(?,?,?)", (int(errorID), str(error), str(e.type)))
        
        if i%100:
            conn.commit()
            print("Processed {}% of the data".format(str((i/183894319)*100)))

    conn.commit()
    conn.close()
    
def build_augmented_dataset(errortype, count, table, cur, conn):
    cur.execute("SELECT errorID from errors where type = '{}'".format(str(errortype)))
    res = cur.fetchall()
    
    print("Processing errortype {}".format(errortype))
    
    counter = 0
    for i in range(len(res)):
        try:
            conn.execute("INSERT INTO {} (input, output, errorID) SELECT input, output, errorID FROM dataset where errorID = {}".format(table, str(res[i][0])))
            counter += 1
        except  sqlite3.Error as er:
            print(er)
            
        if counter > count:
            break
            
    conn.commit()
    
def build_spanish_dataset():
    conn = database_setup()
    cur = conn.cursor()
    
    '''
    10.000 sentences total
    
    500 sentences for all but:
        2000 for ORTH
        2000 for NOUN
        1000 for SPELL
        1000 for PREP
    '''
    
    verb_tense = ["M:VERB:TENSE", "R:VERB:TENSE", "U:VERB:TENSE"]
    very_form = ["M:VERB:FORM", "U:VERB:FORM", "R:VERB:FORM"]
    verb_sva = ["R:VERB:SVA"]
    verb_infl = ["R:VERB:INFL"]
    part = ["M:PART", "R:PART", "U:PART"]
    orth = ["R:ORTH"]
    noun = ["R:NOUN", "M:NOUN", "U:NOUN"]
    noun_num = ["R:NOUN:NUM"]
    noun_infl = ["R:NOUN:INFL"]
    prep = ["U:PREP", "R:PREP", "M:PREP"]
    adv = ["U:ADV" "R:ADV", "M:ADV"]
    spell = ["R:SPELL"]
    pron = ["U:PRON", "R:PRON", "M:PRON"]
    det = ["U:DET", "R:DET", "M:DET"]
    adj = ["U:ADJ", "R:ADJ", "M:ADJ"]
    verb = ["R:VERB", "M:VERB", "U:VERB"]
    punct = ["R:PUNCT", "M:PUNCT", "U:PUNCT"]
    conj = ["U:CONJ", "R:CONJ", "M:CONJ"]
    morph = ["R:MORPH"]
    adj_form = ["R:ADJ:FORM"]
    wo = ["R:WO"]
    
    count500 = []
    count500.append(verb_tense)
    count500.append(very_form)
    count500.append(verb_sva)
    count500.append(verb_infl)
    count500.append(part)
    count500.append(noun_num)
    count500.append(noun_infl)
    count500.append(adv)
    count500.append(pron)
    count500.append(det)
    count500.append(adj)
    count500.append(verb)
    count500.append(punct)
    count500.append(conj)
    count500.append(morph)
    count500.append(adj_form)
    count500.append(wo)

    count2000 = []
    count2000.append(orth)
    count2000.append(noun)
    
    count1000 = []
    count1000.append(spell)
    count1000.append(prep)
    
    for error in count500:
        for error2 in error:
            build_augmented_dataset(error2, 15000/len(error), "spanish", cur, conn)
    
    for error in count1000:
        for error2 in error:
            build_augmented_dataset(error2, 20000/len(error), "spanish", cur, conn)
            
    for error in count2000:
        for error2 in error:
            build_augmented_dataset(error2, 30000/len(error), "spanish", cur, conn)

def build_german_dataset():
    conn = database_setup()
    cur = conn.cursor()
    
    verb_tense = ["M:VERB:TENSE", "R:VERB:TENSE", "U:VERB:TENSE"]
    verb_form = ["M:VERB:FORM", "U:VERB:FORM", "R:VERB:FORM"]
    verb_sva = ["R:VERB:SVA"]
    verb_infl = ["R:VERB:INFL"]
    part = ["M:PART", "R:PART", "U:PART"]
    orth = ["R:ORTH"]
    noun = ["R:NOUN", "M:NOUN", "U:NOUN"]
    noun_num = ["R:NOUN:NUM"]
    noun_infl = ["R:NOUN:INFL"]
    prep = ["U:PREP", "R:PREP", "M:PREP"]
    adv = ["U:ADV" "R:ADV", "M:ADV"]
    spell = ["R:SPELL"]
    pron = ["U:PRON", "R:PRON", "M:PRON"]
    det = ["U:DET", "R:DET", "M:DET"]
    adj = ["U:ADJ", "R:ADJ", "M:ADJ"]
    verb = ["R:VERB", "M:VERB", "U:VERB"]
    punct = ["R:PUNCT", "M:PUNCT", "U:PUNCT"]
    conj = ["U:CONJ", "R:CONJ", "M:CONJ"]
    morph = ["R:MORPH"]
    adj_form = ["R:ADJ:FORM"]
    wo = ["R:WO"]
    
    count500 = []
    count500.append(verb_form)
    count500.append(verb_sva)
    count500.append(verb_infl)
    count500.append(part)
    count500.append(noun_num)
    count500.append(noun_infl)
    count500.append(adv)
    count500.append(pron)
    count500.append(adj)
    count500.append(verb)
    count500.append(punct)
    count500.append(conj)
    count500.append(morph)
    count500.append(adj_form)
    count500.append(wo)

    count2000 = []
    count2000.append(orth)
    
    count1000 = []
    count1000.append(noun)
    count1000.append(verb_tense)
    count1000.append(det)
    count1000.append(noun_num)
    count1000.append(verb_form)
    
    for error in count500:
        for error2 in error:
            build_augmented_dataset(error2, 15000/len(error), "german", cur, conn)
    
    for error in count1000:
        for error2 in error:
            build_augmented_dataset(error2, 20000/len(error), "german", cur, conn)
            
    for error in count2000:
        for error2 in error:
            build_augmented_dataset(error2, 30000/len(error), "german", cur, conn)
            
            
build_spanish_dataset()
'''
Dataset is of type dictionary.
First key in dict: dict_keys(['train'])
First value in dict: dict_values([Dataset({
    features: ['input', 'output'],
    num_rows: 183894319

annotator = errant.load('en')
conn = database_setup()

for i in range(len(dataset['train']['input'])):
    orig = annotator.parse(dataset['train']['input'][i])
    corr = annotator.parse(dataset['train']['output'][i])

    edits = annotator.annotate(orig, corr)
    errorId = int(random.random()*1000000)
    conn.execute("INSERT INTO dataset VALUES(?,?,?)", (str(orig), str(corr), int(errorId)))
    conn.commit()

    for e in edits:
        #print(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str, e.type)
        error = "{} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str)
        conn.execute("INSERT INTO errors VALUES(?,?,?)", (int(errorId), str(error), str(e.type)))
    conn.commit()
    print("Processed {}% of the data".format(str((i/len(dataset['train']['input'])*100))))
    '''

'''
annotator = errant.load('en')
orig = annotator.parse("I think I'm goign to have to inform your posts a few times in order to gain even a small appreciation on how it all fits togethed but it looks very interesting!")
corr = annotator.parse("I think I'm going to have to re-read your posts a few times in order to gain even a small understanding of how it all fits together but it certainly looks very interesting!")

edits = annotator.annotate(orig, corr)
for e in edits:
    #print(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str, e.type)
    #error = "{} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str)
    print(e.type)
'''
