from happytransformer import HappyTextToText
from happytransformer import TTTrainArgs
from happytransformer import TTSettings
from happytransformer import HappyGeneration
import sqlite3
import csv
import errant

def database_setup():
    conn = sqlite3.connect("c4_200m_errors.db")
    conn.execute("CREATE TABLE IF NOT EXISTS dataset(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS errors(errorID INTEGER, error VARCHAR, type VARCHAR)")
    conn.execute("CREATE TABLE IF NOT EXISTS spanish(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.execute("CREATE TABLE IF NOT EXISTS german(input VARCHAR, output VARCHAR, errorID INTEGER UNIQUE)")
    conn.commit()
    return conn


def create_dataset(table):
    conn = database_setup()
    cur = conn.cursor()
    cur.execute("SELECT input, output from {}".format(table))
    res = cur.fetchall()
    
    #threshold = 3/4*len(res)
    threshold = 4/5*len(res)
    index = 0
    train = True
    
    with open("train_{}.csv".format(table), 'w', newline='') as traincsv:
        with open("test_{}.csv".format(table), 'w', newline='') as testcsv:
            writer_train = csv.writer(traincsv)
            writer_test = csv.writer(testcsv)
            writer_train.writerow(["input", "target"])
            writer_test.writerow(["input", "target"])
            
            for pair in res:
                input = pair[0]
                target = pair[1]
                
                # adding the task name
                input = "grammar: " + input
                
                if train:
                    writer_train.writerow([input, target])
                else:
                    writer_test.writerow([input, target])
        
                index+=1
                if index > threshold:
                    train = False

def train(lang):   
    happy_tt = HappyTextToText("T5", "t5-base")
    before_result = happy_tt.eval("test_{}.csv".format(lang))
    print("Before loss:", before_result.loss)
    
    args = TTTrainArgs(batch_size=2, fp16=True)
    happy_tt.train("test_{}.csv".format(lang), args=args)
    
    after_loss = happy_tt.eval("test_{}.csv".format(lang))
    print("After loss: ", after_loss.loss)
    
    beam_settings =  TTSettings(num_beams=5, min_length=1, max_length=20)
    example_1 = "grammar: This sentences, has bads grammar and spelling!"
    result_1 = happy_tt.generate_text(example_1, args=beam_settings)
    print(result_1.text)
    
    example_2 = "grammar: I am enjoys, writtings articles ons AI."
    result_2 = happy_tt.generate_text(example_2, args=beam_settings)
    print(result_2.text)
    
    happy_tt.save("t5-{}/".format(lang))
    
def inference(lang):
    happy_tt = HappyTextToText("T5", "t5-base", load_path="t5-{}/".format(lang))
    beam_settings =  TTSettings(num_beams=5, min_length=1, max_length=20)
    annotator = errant.load('en')
    
    while True:
        print("Enter text:")
        text = input()
        output = happy_tt.generate_text(text, args=beam_settings)
        
        orig = annotator.parse(text)
        corr = annotator.parse(output.text)
        edits = annotator.annotate(orig, corr)
        
        print(output.text)
        for e in edits:
            error = "{} {} {} {} {} {} {}".format(e.o_start, e.o_end, e.o_str, e.c_start, e.c_end, e.c_str, e.type)
            print(error)

create_dataset("spanish")
train("spanish")
#inference('spanish')